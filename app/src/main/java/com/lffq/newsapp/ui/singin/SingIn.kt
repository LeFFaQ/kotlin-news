package com.lffq.newsapp.ui.singin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.lffq.newsapp.R
import com.lffq.newsapp.ui.singup.SingUp
import com.lffq.newsapp.databinding.ActivitySingInBinding

class SingIn : AppCompatActivity() {

    lateinit var binding: ActivitySingInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel: SingInViewModel by viewModels()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sing_in)
        binding.viewmodel = viewModel
    }

    fun moveToUp() {
        startActivity(Intent(this, SingUp::class.java))
    }

//    fun singin(view: View) {
//
//        var mAuth: FirebaseAuth
//        mAuth = FirebaseAuth.getInstance()
//        Log.d(ContentValues.TAG, binding.email.toString())
//        mAuth.signInWithEmailAndPassword(binding.email.toString(), binding.password.toString())
//            .addOnCompleteListener(this,
//                OnCompleteListener<AuthResult?> { task ->
//                    if (task.isSuccessful) {
//                        // Sign in success, update UI with the signed-in user's information
//                        Log.d(ContentValues.TAG, "signInWithEmail:success")
//                        val user = mAuth.currentUser
//                    } else {
//                        // If sign in fails, display a message to the user.
//                        Log.w(ContentValues.TAG, "signInWithEmail:failure", task.exception)
//                        Toast.makeText(
//                            this, "Authentication failed.",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    }
//
//                    // ...
//                })
//    }


}