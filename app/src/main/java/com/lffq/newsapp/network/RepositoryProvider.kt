package com.lffq.newsapp.network

object RepositoryProvider {
    fun provideSearchRepository(): SearchRepository {
        return SearchRepository(NewsApi.create())
    }
}