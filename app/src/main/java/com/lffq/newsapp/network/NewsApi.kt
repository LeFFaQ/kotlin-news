package com.lffq.newsapp.network

import com.lffq.newsapp.network.models.News
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    @GET("search/users")
    fun search(
        @Query("q") query: String,
        @Query("language") lang: String,
        @Query("apiKey") key: String): Observable<News>


    companion object Factory {
        fun create(): NewsApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://newsapi.org/")
                .build()

            return retrofit.create(NewsApi::class.java);
        }
    }
}