package com.lffq.newsapp.network

import com.lffq.newsapp.network.models.News
import io.reactivex.Observable


class SearchRepository(val apiService: NewsApi) {
    fun searchUsers(q: String, lang: String, apikey: String): Observable<News> {
        return apiService.search(q, lang, apikey)
    }
}